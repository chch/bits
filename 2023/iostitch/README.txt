i/o +stitch
-----------

Lines are code and strings are attached! Being invited to the 'Schiassn' and their Brother Stitching Machine we will explore possible worlds of free/libre/open source graphic making.

At the center of our investigation are Scalable Vector Graphics (SVG), a two-dimensional image format developed as an open standard by the World Wide Web Consortium since 1999. The free and open-source vector graphics editor Inkscape provides a graphical user interface to this text-based file format but there are a lot of different ways to work with SVG files basically through any application that is capable of manipulating plain text.

Throughout the workshop we will visit different workflows and possibilities around Scalable Vector Graphics. This includes hands-on introductions to Inkscape, open-source machine embroidery, graphic processing on the commandline and micro-politics of interfaces.

Regardless if you consider yourself a graphic person or a GNU/Linux aficionado or something very different we want to invite you to explore unknown territories with us! Open for anyone. Bring a computer if you have one.

----

Christoph Haag works as a design practitioner and editor across different fields of visual cultures and technologies.

Design Practice as feedback loops of using and making, transforming requirements into prereqisites and back again. Technologies not as instruments for streamlined production, but as creative matter to engage with. Maybe in a spirit of artisanal nostalgia. Definitely towards questions of literacy in relation to contemporary ideas of convenience.

As a user and maker of digital infrastructures he has a particular interest in free/libre/open source methodologies, both theory and practice.

--

Johannes Gusenleitner is an occupational therapist working with mentally ill inmates in a forensic-therapeutic setting. Using arts and crafts to keep, train or regain skills is a fundamental part of the therapeutic work with imprisoned clients.

He is taking care of the sewing studio at Kulturverein zur Schießhalle.
